package co.istad.elearningapp.service;

import co.istad.elearningapp.config.WebClientConfig;
import co.istad.elearningapp.response.ApiResponse;
import co.istad.elearningapp.response.Paging;
import co.istad.elearningapp.response.Topic;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TopicServiceImpl implements TopicService {

    private final WebClientConfig webClient;

    @Override
    public ApiResponse<Paging<Topic>> findAllTopics(Integer pageNum) {
        return webClient.getTopicClient().findAllTopics(pageNum,4);
    }
}
