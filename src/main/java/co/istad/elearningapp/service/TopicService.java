package co.istad.elearningapp.service;

import co.istad.elearningapp.response.ApiResponse;
import co.istad.elearningapp.response.Paging;
import co.istad.elearningapp.response.Topic;

public interface TopicService {

    ApiResponse<Paging<Topic>> findAllTopics(Integer pageNum);

}
