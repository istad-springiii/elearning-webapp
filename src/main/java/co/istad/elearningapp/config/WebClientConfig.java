package co.istad.elearningapp.config;

import co.istad.elearningapp.api.TopicClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.support.WebClientAdapter;
import org.springframework.web.service.invoker.HttpServiceProxyFactory;

@Component
public class WebClientConfig {

    private final TopicClient topicClient;

    public TopicClient getTopicClient() {
        return topicClient;
    }

    public WebClientConfig() {

        final String BASE_URL = "https://elearning.istad.co/api/v1/";

        WebClient webClient = WebClient.builder()
                .baseUrl(BASE_URL)
                .defaultHeader(
                        "Authorization",
                        "Basic Y2hhbmNoaGF5YTpJU1RBREAyMDIz"
                )
                .build();

        HttpServiceProxyFactory httpServiceProxyFactory =
                HttpServiceProxyFactory.builder(WebClientAdapter.forClient(webClient))
                        .build();

        topicClient = httpServiceProxyFactory.createClient(TopicClient.class);
    }

}
