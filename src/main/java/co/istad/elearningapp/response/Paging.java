package co.istad.elearningapp.response;

import lombok.Data;

import java.util.List;

@Data
public class Paging<T> {
    private List<T> list;
    private Integer total;
    private Integer pageNum;
    private Integer[] navigatepageNums;
}
