package co.istad.elearningapp.response;

public record Topic(Integer id,
                    String name,
                    String description) {
}
