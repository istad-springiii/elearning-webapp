package co.istad.elearningapp.response;

import lombok.Data;

import java.time.LocalDateTime;

@Data
public class ApiResponse<T> {
    private String message;
    private Boolean status;
    private LocalDateTime requestedTime;
    private T data;
}
