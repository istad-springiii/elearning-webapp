package co.istad.elearningapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ElearningWebappApplication {

	public static void main(String[] args) {
		SpringApplication.run(ElearningWebappApplication.class, args);
	}

}
