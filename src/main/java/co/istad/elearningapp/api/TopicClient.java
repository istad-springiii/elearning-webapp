package co.istad.elearningapp.api;

import co.istad.elearningapp.response.ApiResponse;
import co.istad.elearningapp.response.Paging;
import co.istad.elearningapp.response.Topic;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.service.annotation.GetExchange;

import java.util.List;

public interface TopicClient {

    @GetExchange("topics")
    ApiResponse<Paging<Topic>> findAllTopics(@RequestParam("pageNum") int pageNum,
                                             @RequestParam("pageSize") int pageSize);

}
