package co.istad.elearningapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
@Controller
public class CourseController {

    @GetMapping("/course")
    String viewCourse(Model model) {

        boolean isSucceed = true;
        String htmlCode = "<h1>Course Page</h1>";

        model.addAttribute("heading", htmlCode);
        model.addAttribute("status", isSucceed);

        return "page/course/course";
    }

    @GetMapping("/course/{id}")
    String viewCourseById(@PathVariable Integer id, Model model) {
        model.addAttribute("id", id);
        return "page/course/course-detail";
    }

}
