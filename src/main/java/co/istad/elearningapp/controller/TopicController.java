package co.istad.elearningapp.controller;

import co.istad.elearningapp.service.TopicServiceImpl;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequiredArgsConstructor
public class TopicController {

    private final TopicServiceImpl topicService;

    @GetMapping("/topic")
    String viewTopic(@RequestParam(required = false, defaultValue = "1") Integer pageNum, Model model) {

        model.addAttribute("data", topicService.findAllTopics(pageNum));

        return "page/topic";
    }
}
