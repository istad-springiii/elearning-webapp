package co.istad.elearningapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class InfoController {

    @GetMapping("/aboutus")
    String viewTopic() {
        return "page/aboutus";
    }
}
