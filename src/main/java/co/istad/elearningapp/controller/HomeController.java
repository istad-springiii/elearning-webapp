package co.istad.elearningapp.controller;

import co.istad.elearningapp.response.Topic;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.ArrayList;
import java.util.List;

@Controller
public class HomeController {

    @GetMapping("/")
    String viewIndex(Model model) {

        List<Topic> topics = new ArrayList<>();

        Topic topic1 = new Topic(1, "Back-End", "Back-End with Spring Boot");
        Topic topic2 = new Topic(2, "Front-End", "Front-End with ReactJS");
        Topic topic3 = new Topic(3, "Mobile Dev.", "Mobile Dev. with Flutter");
        Topic topic4 = new Topic(4, "Web Dev.", "Web Fullstack Dev.");
        Topic topic5 = new Topic(5, "DevOps Eng.", "Development + Operation");

        topics.add(topic1);
        topics.add(topic2);
        topics.add(topic3);
        topics.add(topic4);
        topics.add(topic5);

        model.addAttribute("name", "Kimly");
        model.addAttribute("topics", topics);
        return "index";
    }
}
